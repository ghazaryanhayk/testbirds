// Constant values
export const MAX_VISIBLE_COUNT = 5;
export const ASTERISK_VALUE = 'External';

// Non-dynamic text elements
export const TITLE = 'Your team for this test';
export const SHOW_ALL = 'Show all';
export const SHOW_LESS = 'Show less';
export const DROP_DOWN_HANDLE = 'Add team member <br/> to this list';
export const DROP_DOWN_NOT_FOUND_PRIMARY = 'Team member not found.';
export const DROP_DOWN_NOT_FOUND_SECONDARY = 'Maybe she/he is not yet in your team?';
export const DROP_DOWN_SEARCH_PLACEHOLDER = 'Search...';
export const USER_REMOVE = 'Remove user';
export const USER_AVATAR = 'Avatar';
export const NOT_SUPPORTED_BROWSER = 'Sorry, your browser is too old :(';

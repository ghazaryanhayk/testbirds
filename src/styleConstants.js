// breakpoints for media queries
export const breakpoints = {
  small: 768,
  medium: 1300,
  large: 1900
};

// widths
export const wrapperSmallSize = '330px';
export const wrapperLargeSize = '535px';

// margins
export const marginExtraLarge = '20px';
export const marginLarge = '16px';
export const marginMedium = '10px';
export const marginSmall = '8px';

// colors
export const primaryGreen = '#007672';
export const lightGreen = '#e2f4ea';
export const textDarkGreen = '#18332f';
export const borderGray = '#d0d3d4';
export const textGray = '#999999';
export const white = '#ffffff';
export const orange = '#fd712f';

// fonts
export const largeFontSize = '15px';
export const mediumFontSize = '12px';
export const smallFontSize = '10px';
export const normalWeight = 400;
export const semibold = 600;
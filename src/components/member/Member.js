import React from 'react';
import {
  memberImage,
  memberName,
  memberRole,
  memberWrapper,
  memberRemove,
  asterisk
} from "./Member.style";
import { ASTERISK_VALUE, USER_AVATAR } from "../../constants";

const Member = (props) => {
  return (
    <div className={memberWrapper}>
      <div
        className={memberRemove}
        onClick={() => props.removeHandler(props.member.id)}
      >
        <div>+</div>
      </div>
      <img
        src={`/${props.member.picture}`}
        className={memberImage}
        alt={USER_AVATAR}
      />
      <div>
        <div className={memberRole}>
          {props.member.role}
          {
            props.member.role === ASTERISK_VALUE &&
            <span className={asterisk}> *</span>
          }
        </div>
        <div className={memberName}>
          {props.member.username}
        </div>
      </div>
    </div>
  );
};

export default Member;
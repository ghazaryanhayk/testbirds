import { css } from 'react-emotion';
import {
  lightGreen,
  textDarkGreen,
  white,
  orange,
  breakpoints,
  largeFontSize,
  normalWeight,
  semibold,
  marginMedium,
  marginLarge,
  smallFontSize
} from "../../styleConstants";
import { USER_REMOVE } from "../../constants";

export const memberWrapper = css`
	display: flex;
	align-items: center;
	transition: background-color .3s;
	
	&:hover {
		background-color: ${lightGreen};

		& > img {
			display: none;
		}

		& > div:first-child {
			display: block;
		}
	}
	
	@media
		screen and (min-width: ${breakpoints.small}px) and (max-width: ${breakpoints.medium}px),
		screen and (min-width: ${breakpoints.large}px)
	{
		flex-basis: 50%;
	}
`;

export const memberRemove = css`
	position: relative;
	display: none;
	flex-basis: 40px;
	flex-grow: 0;
	flex-shrink: 0;
	max-width: 40px;
	height: 40px;
	margin: ${marginMedium};
	line-height: 40px;
	text-align: center;
	font-size: 30px;
	background-color: ${white};
	border: 1px solid ${white};
	border-radius: 20px;
	box-sizing: border-box;
	cursor: pointer;
	transition: all .3s;
	
	@media
		screen and (min-width: ${breakpoints.small}px) and (max-width: ${breakpoints.medium}px),
		screen and (min-width: ${breakpoints.large}px)
	{
		margin: ${marginLarge} ${marginMedium};
	}

	div {
		transform: rotate(45deg);
		color: ${orange};
	}

	&:hover {
		border: 1px solid ${orange};

		&:before {
			content: '${USER_REMOVE}';
			background-color: ${white};
			color: ${textDarkGreen};
			position: absolute;
			top: -30px;
			font-size: ${smallFontSize};
			width: 80px;
			line-height: 20px;
			height: 20px;
			left: -20px;
			border-radius: 2px;
			-webkit-box-shadow: 0px 2px 5px 1px rgba(191,191,191,1);
			-moz-box-shadow: 0px 2px 5px 1px rgba(191,191,191,1);
			box-shadow: 0px 2px 5px 1px rgba(191,191,191,1);
			transition: all .3s;
		}

		&:after {
			content: '';
			position: absolute;
			top: -15px;
			height: 8px;
			width: 8px;
			left: 15px;
			background-color: ${white};
			transform: rotate(45deg);
			transition: all .3s;
		}
	}
`;

export const memberImage = css`
	max-width: 40px;
	max-height: 40px;
	flex-basis: 40px;
	flex-grow: 0;
	flex-shrink: 0;
	border-radius: 20px;
	margin: ${marginMedium};
	@media
		screen and (min-width: ${breakpoints.small}px) and (max-width: ${breakpoints.medium}px),
		screen and (min-width: ${breakpoints.large}px)
	{
		margin: ${marginLarge} ${marginMedium};
	}
`;

export const memberRole = css`
	font-size: ${largeFontSize};
	font-weight: ${normalWeight};
	color: ${textDarkGreen}
`;

export const memberName = css`
	font-size: ${largeFontSize};
	font-weight: ${semibold};
	color: ${textDarkGreen}
`;

export const asterisk = css`
	color: ${orange};
`;
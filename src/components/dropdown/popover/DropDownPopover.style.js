import { css } from 'react-emotion';
import { white } from '../../../styleConstants';

export const dropDownPopoverWrapper = css`
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	z-index: 1;
	background-color: ${white};
	-webkit-box-shadow: 0px 2px 5px 1px rgba(191,191,191,1);
	-moz-box-shadow: 0px 2px 5px 1px rgba(191,191,191,1);
	box-shadow: 0px 2px 5px 1px rgba(191,191,191,1);
`;
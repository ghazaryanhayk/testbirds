import React, { Component } from 'react';
import DropDownInput from "./input/DropDownInput";
import DropDownList from "./list/DropDownList";
import { dropDownPopoverWrapper } from './DropDownPopover.style';

class DropDownPopover extends Component {
  constructor (props) {
    super(props);

    this.wrapperRef = null;

    this.getWrapperRef = this.getWrapperRef.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
  }

  componentDidMount () {
    document.addEventListener('mousedown', this.handleOutsideClick);
  }

  componentWillUnmount () {
    document.removeEventListener('mousedown', this.handleOutsideClick);
  }

  getWrapperRef (ref) {
    this.wrapperRef = ref;
  }

  handleOutsideClick (e) {
    if (this.wrapperRef && !this.wrapperRef.contains(e.target)) {
      this.props.handleClose();
    }
  }

  render () {
    return (
      <div
        className={dropDownPopoverWrapper}
        ref={this.getWrapperRef}
      >
        <DropDownInput filterHandler={this.props.filterHandler}/>
        <DropDownList
          filteredMembers={this.props.filteredMembers}
          handleSelect={this.props.handleSelect}
          isDirty={this.props.isDirty}
        />
      </div>
    );
  }
}

export default DropDownPopover;
import { css } from 'react-emotion';
import {
  largeFontSize,
  marginLarge,
  primaryGreen,
  textDarkGreen,
  semibold,
  smallFontSize,
} from '../../../../../styleConstants';

export const dropDownNotFoundWrapper = css`
	padding: 0 ${marginLarge} ${marginLarge} ${marginLarge};
	text-align: center;
`;

export const dropDownNotFoundPrimary = css`
	font-size: ${largeFontSize};
	color: ${primaryGreen};
	font-weight: ${semibold};
`;
export const dropDownNotFoundSecondary = css`
	font-size: ${smallFontSize};
	color: ${textDarkGreen};
`;
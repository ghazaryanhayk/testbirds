import React from 'react';
import {
  DROP_DOWN_NOT_FOUND_PRIMARY,
  DROP_DOWN_NOT_FOUND_SECONDARY
} from "../../../../../constants";
import {
  dropDownNotFoundPrimary,
  dropDownNotFoundSecondary,
  dropDownNotFoundWrapper
} from './DropDownNotFound.style';

const DropDownNotFound = () => {
  return (
    <div className={dropDownNotFoundWrapper}>
      <div className={dropDownNotFoundPrimary}>
        {DROP_DOWN_NOT_FOUND_PRIMARY}
      </div>
      <div className={dropDownNotFoundSecondary}>
        {DROP_DOWN_NOT_FOUND_SECONDARY}
      </div>
    </div>
  );
};

export default DropDownNotFound;
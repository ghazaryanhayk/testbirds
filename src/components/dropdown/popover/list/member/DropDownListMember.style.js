import { css } from 'react-emotion';
import {
  largeFontSize,
  lightGreen,
  marginMedium,
  marginSmall,
  semibold
} from '../../../../../styleConstants';

export const dropDownListMember = css`
	display: flex;
	align-items: center;
	cursor: pointer;
	transition: background-color .3s;
	
	&:hover {
		background-color: ${lightGreen};
	}
	
	img {
		flex-basis: 30px;
		max-width: 30px;
		max-height: 30px;
		margin: ${marginSmall} ${marginMedium};
		border-radius: 15px;
	}
	
	div {
		font-size: ${largeFontSize};
		font-weight: ${semibold};
	}
`;
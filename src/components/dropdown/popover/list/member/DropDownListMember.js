import React from 'react';
import { dropDownListMember } from './DropDownListMember.style';

const DropDownListMember = (props) => {
  return (
    <div
      className={dropDownListMember}
      onClick={() => props.handleSelect(props.member.id)}
    >
      <img
        src={`/${props.member.picture}`}
        alt={props.member.username}
      />
      <div>
        {props.member.username}
      </div>
    </div>
  );
};

export default DropDownListMember;
import React from 'react';
import DropDownListMember from "./member/DropDownListMember";
import DropDownNotFound from "./not-found/DropDownNotFound";

const DropDownList = (props) => {
  return (
    <div>
      {
        props.filteredMembers.length === 0 &&
        props.isDirty &&
        <DropDownNotFound/>
      }
      {
        props.filteredMembers.map((member) => {
          return (
            <DropDownListMember
              handleSelect={props.handleSelect}
              member={member}
              key={member.id}
            />
          );
        })
      }
    </div>
  );
};

export default DropDownList;
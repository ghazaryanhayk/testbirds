import { css } from 'react-emotion';
import { largeFontSize, orange, primaryGreen, semibold } from '../../../../styleConstants';

export const dropDownInput = css`
	border-width: 0 0 1px 0;
	border-color: ${primaryGreen};
	border-style: solid;
	font-size: ${largeFontSize};
	font-weight: ${semibold};
	color: ${primaryGreen};
	outline: none;
	padding: 6px 26px 6px 6px;
	width: 100%;
	box-sizing: border-box;
`;

export const dropDownClear = css`
	position: absolute;
	top: 20px;
	right: 20px;
	transform: rotate(45deg);
	color: ${orange};
	cursor: pointer;
	font-size: 20px;
`;
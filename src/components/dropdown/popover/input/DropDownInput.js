import React, { Component } from 'react';
import { dropDownInputWrapper } from "../../DropDown.style";
import { DROP_DOWN_SEARCH_PLACEHOLDER } from "../../../../constants";
import { dropDownClear, dropDownInput } from './DropDownInput.style';

class DropDownInput extends Component {
  constructor (props) {
    super(props);

    this.state = {
      searchValue: ''
    };

    this.inputRef = null;
    this.handleChange = this.handleChange.bind(this);
    this.getInputRef = this.getInputRef.bind(this);
    this.handleClear = this.handleClear.bind(this);
  }

  getInputRef (ref) {
    this.inputRef = ref;
  }

  componentDidMount () {
    this.inputRef.focus();
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.searchValue !== prevState.searchValue) {
      prevProps.filterHandler(this.state.searchValue);
    }
  }

  handleChange (e) {
    e.preventDefault();
    this.setState({
      searchValue: e.target.value
    });
  }

  handleClear () {
    this.setState({
      searchValue: ''
    });
  }

  render () {
    return (
      <div className={dropDownInputWrapper}>
        <input
          className={dropDownInput}
          type="text"
          onChange={this.handleChange}
          placeholder={DROP_DOWN_SEARCH_PLACEHOLDER}
          ref={this.getInputRef}
          value={this.state.searchValue}
        />
        <a
          className={dropDownClear}
          onClick={this.handleClear}
        >+</a>
      </div>
    );
  }
}

export default DropDownInput;
import React, { Component } from 'react';
import {
  dropDownHandler,
  dropDownPlus,
  dropDownText,
  dropDownWrapper
} from "./DropDown.style";
import DropDownPopover from "./popover/DropDownPopover";
import { DROP_DOWN_HANDLE } from "../../constants";

class DropDown extends Component {

  constructor (props) {
    super(props);

    this.state = {
      isDirty: false,
      isOpen: false,
      filteredMembers: []
    };

    this.filter = this.filter.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleOpen () {
    this.setState({
      isOpen: true
    });
  }

  handleSelect (id) {
    this.handleClose();
    this.props.handleSelect(id);
  }

  handleClose () {
    this.setState({
      isDirty: false,
      isOpen: false,
      filteredMembers: []
    });
  }

  filter (value) {
    let filteredItems = this.props.itemsList.filter((item) => {
      return item.username.toLowerCase().indexOf(value.toLowerCase()) > -1;
    });

    this.setState({
      isDirty: true,
      filteredMembers: filteredItems
    });
  }

  render () {
    return (
      <div className={dropDownWrapper}>
        <div onClick={this.handleOpen} className={dropDownHandler}>
          <span className={dropDownPlus}>+</span>
          <span
            className={dropDownText}
            dangerouslySetInnerHTML={{ __html: DROP_DOWN_HANDLE }}
          ></span>
        </div>
        {
          this.state.isOpen &&
          <DropDownPopover
            isDirty={this.state.isDirty}
            filterHandler={this.filter}
            filteredMembers={this.state.filteredMembers}
            handleSelect={this.handleSelect}
            handleClose={this.handleClose}
          />
        }
      </div>
    );
  }
}

export default DropDown;

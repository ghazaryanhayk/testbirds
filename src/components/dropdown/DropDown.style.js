import { css } from 'react-emotion';
import {
  breakpoints, largeFontSize,
  lightGreen,
  marginLarge,
  marginMedium,
  primaryGreen,
  semibold,
  white,
} from "../../styleConstants";

export const dropDownWrapper = css`
	position: relative;
	
	@media
		screen and (min-width: ${breakpoints.small}px) and (max-width: ${breakpoints.medium}px),
		screen and (min-width: ${breakpoints.large}px) {
		flex-basis: 50%;
	}
`;

export const dropDownHandler = css`
	display: flex;
	align-items: center;
	cursor: pointer;
	transition: background-color .3s;
	
	&:hover {
		background-color: ${lightGreen};

		span:first-child {
			background-color: ${white};
		}
	}
`;

export const dropDownPlus = css`
	flex-basis: 40px;
	flex-shrink: 0;
	flex-grow: 0;
	height: 40px;
	border-radius: 20px;
	background-color: ${lightGreen};
	text-align: center;
	font-size: 30px;
	line-height: 40px;
	margin: ${marginMedium};
	transition: background-color .3s;
	color: ${primaryGreen};
	
	@media
		screen and (min-width: ${breakpoints.small}px) and (max-width: ${breakpoints.medium}px),
		screen and (min-width: ${breakpoints.large}px)
	{
		margin: ${marginLarge} ${marginMedium};
	}
`;

export const dropDownText = css`
	font-size: ${largeFontSize};
	font-weight: ${semibold};
	line-height: 1.4;
	color: ${primaryGreen};
`;

export const dropDownInputWrapper = css`
	padding: ${marginLarge} ${marginMedium};
`;
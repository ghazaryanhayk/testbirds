import styled, { css } from 'react-emotion';
import {
  borderGray,
  lightGreen,
  primaryGreen,
  textDarkGreen,
  textGray,
  breakpoints,
  marginExtraLarge,
  wrapperLargeSize,
  wrapperSmallSize, semibold, marginLarge, mediumFontSize,
} from "./styleConstants";

export const appStyle = css`
	box-sizing: border-box;
	width: ${wrapperSmallSize};
	margin: auto;
	
	border: 1px solid ${borderGray};
	border-top: 3px solid ${primaryGreen};
	
	@media
		screen and (min-width: ${breakpoints.small}px) and (max-width: ${breakpoints.medium}px),
		screen and (min-width: ${breakpoints.large}px)
	{
		width: ${wrapperLargeSize};
	}
`;

export const heading = css`
	font-size: 14px;
	line-height: 1;
	color: ${textGray};
	font-weight: ${semibold};
	text-transform: uppercase;
	margin: 0;
	padding: ${marginExtraLarge} ${marginExtraLarge} ${marginLarge} ${marginExtraLarge};
`;

export const membersWrapper = css`
	display: flex;
	flex-direction: column;
	padding: 0 ${marginExtraLarge} ${marginExtraLarge} ${marginExtraLarge};
	
	@media
		screen and (min-width: ${breakpoints.small}px) and (max-width: ${breakpoints.medium}px),
		screen and (min-width: ${breakpoints.large}px)
	{
		flex-wrap: wrap;
		flex-direction: row;
	}
`;

export const ShowAllStyled = styled('div')`
	background-color: ${lightGreen};
	font-size: ${mediumFontSize};
	text-align: center;
	height: 38px;
	line-height: 38px;
	cursor: pointer;
	color: ${textDarkGreen};
	
	&:after {
		content: '';
		display: inline-block;
		border-width: 0 0 1px 1px;
		border-style: solid;
		border-color: ${textDarkGreen};
		width: 8px;
		height: 8px;
		transform: ${
  props =>
    props.isCollapsed
      ? 'rotate(-45deg) translate(30%, 0)'
      : 'rotate(135deg) translate(0, -50%)'
  };
		margin-left: 10px;
		margin-top: -3px;
	}
`;
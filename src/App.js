import React, { Component } from 'react';
import { appStyle, heading, membersWrapper, ShowAllStyled } from './App.style';
import DropDown from "./components/dropdown/DropDown";
import items from './data.json';
import { MAX_VISIBLE_COUNT, NOT_SUPPORTED_BROWSER, SHOW_ALL, SHOW_LESS, TITLE } from "./constants";
import Member from "./components/member/Member";

class App extends Component {

  constructor (props) {
    super(props);

    let members;

    if (window.localStorage && window.localStorage.getItem('members') !== null) {
      members = JSON.parse(window.localStorage.getItem('members'));
    } else {
      members = {
        selectedMembers: [],
        remainingMembers: items,
      };
      window.localStorage.setItem('members', JSON.stringify(members));
    }

    this.state = {
      ...members,
      isCollapsed: true
    };

    this.selectMember = this.selectMember.bind(this);
    this.removeMember = this.removeMember.bind(this);
    this.updateData = this.updateData.bind(this);
    this.toggleShowAll = this.toggleShowAll.bind(this);
    this.generateMembersList = this.generateMembersList.bind(this);
  }

  selectMember (id) {
    let member = this.state.remainingMembers.find((member) => {
      return member.id === id;
    });

    let selectedMembers = this.state.selectedMembers;
    selectedMembers.unshift(member);

    let remainingMembers = this.state.remainingMembers.filter((member) => {
      return member.id !== id;
    });

    this.updateData(selectedMembers, remainingMembers);
  }

  removeMember (id) {
    let member = this.state.selectedMembers.find((member) => {
      return member.id === id;
    });

    let remainingMembers = this.state.remainingMembers;
    remainingMembers.push(member);

    let selectedMembers = this.state.selectedMembers.filter((member) => {
      return member.id !== id;
    });

    this.updateData(selectedMembers, remainingMembers);
  }

  updateData (selectedMembers, remainingMembers) {
    const members = {
      selectedMembers: selectedMembers,
      remainingMembers: remainingMembers,
    };
    this.setState(members);
    window.localStorage.setItem('members', JSON.stringify(members));
  }

  toggleShowAll () {
    this.setState({
      isCollapsed: !this.state.isCollapsed
    });
  }

  generateMembersList () {

    let selectedMembers =
      (this.state.isCollapsed)
        ? this.state.selectedMembers.slice(0, MAX_VISIBLE_COUNT)
        : this.state.selectedMembers;

    return selectedMembers.map((member) => {
      return <Member
        key={member.id}
        member={member}
        removeHandler={this.removeMember}
      />
    });
  }

  render () {
    if (!window.localStorage) {
      return (
        <div>{NOT_SUPPORTED_BROWSER}</div>
      );
    }

    return (
      <div className={appStyle}>
        <h1 className={heading}>
          {TITLE}
        </h1>
        <div className={membersWrapper}>
          <DropDown
            itemsList={this.state.remainingMembers}
            handleSelect={this.selectMember}
          />
          {this.generateMembersList()}
        </div>
        {
          this.state.selectedMembers.length > MAX_VISIBLE_COUNT &&
          <ShowAllStyled
            onClick={this.toggleShowAll}
            isCollapsed={this.state.isCollapsed}
          >
            {
              this.state.isCollapsed ? SHOW_ALL : SHOW_LESS
            }
          </ShowAllStyled>
        }
      </div>
    );
  }
}

export default App;

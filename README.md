# Testbirds Front-End challenge

Steps to see application

* clone [https://bitbucket.org/ghazaryanhayk/testbirds](https://bitbucket.org/ghazaryanhayk/testbirds) repository
* run ```npm install```
* run ```npm start```

On first time run application will get data from ```data.json``` and put into ```localStorage```.
Next times it will use data from localStorage and will keep application state.

Tested on latest versions of following browsers

* Microsoft Edge
* Google Chrome
* Mozilla Firefox
* Safari
* Google Chrome (Android, iOS)

## Folder structure

Root component (App.js) is located in /src directory. All other components are located in
/src/components hierarchically.

Every component has it's style file included in the same directory.

## Used external modules

* emotion
* react-emotion

## Notes

"Team page" button is not created, as it's has no functionality in this challenge.